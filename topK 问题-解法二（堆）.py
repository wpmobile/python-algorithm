# coding:utf-8
import random
import time
import heapq
n = 10000 * 1000
k = 10
print(n)


def gen_num(n):
    for i in range(n):
        yield random.randint(0, n)


l1 = list(gen_num(n))

start = time.time()
result = heapq.nlargest(k, l1)
print (result)
print(time.time()-start)

# 总结：海量数据top K问题，最典型的解法就是用 堆  这种数据结构。效率很高