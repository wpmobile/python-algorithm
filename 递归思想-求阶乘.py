# coding:utf-8
# 编写递归函数有两个要点：
#
# 1. 出口条件，可以不止一个
# 2. 推导方法（已知上一个结果怎么推导当前结果）

# 题目：求n的阶乘


def factorial(n):
    if n == 1:  # 1.出口
        return 1
    return factorial(n-1) * n   # 2.自我调用求上一个结果，然后推导本层结果


print (factorial(5))



