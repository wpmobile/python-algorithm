# coding:utf-8
import random
import time
n = 10000 * 1000
k = 10
print(n)


def gen_num(n):
    for i in range(n):
        yield random.randint(0, n)


l1 = gen_num(n)
# print len(list(l1)) l1现在应该是个迭代器，可以list转换成列表
start = time.time()
l2 = list(set(l1))
result = l2[-k:]
result.reverse()
print(result)
print(time.time()-start)
