# coding = utf-8
# author  WangLei

'''
求1~1000的质数.
质数（prime number）又称素数，有无限个。质数定义为在大于1的自然数中，除了1和它本身以外不再有其他因数的数称为质数
'''

# 定义一个列表
num = []
# 遍历查找范围
for i in range(2,1000):
    # 遍历因数
    # for j in range(2,i):    优化如下
    for j in range(2, i//2 + 1):
        if i % j == 0:
            break
    else:
        num.append(i)
print(num)

# 注意以下问题，会生成nothing，直接跳出循环
for y in range(2,2):
    print(y)
